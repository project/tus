<?php
namespace Drupal\tus;

use Drupal\Core\Database\Connection;
use Drupal\Core\TempStore\SharedTempStore;
use TusPhp\Cache\AbstractCache;

class  DrupalTempStoreCache extends AbstractCache {

  public function __construct(SharedTempStore $tempStore, Connection $db) {
    $this->tempStore = $tempStore;
    $this->db = $db;
  }

  public function get(string $key, bool $withExpired = FALSE) {
    return $this->tempStore->get($key);
  }

  /**
   * add the value being updated to the data
   *
   * @param string $key
   * @param $value
   *
   * @return mixed|void
   * @throws \Drupal\Core\TempStore\TempStoreException
   */
  public function set(string $key, $value) {
    // when setting we need to get the cache
    $data = $this->get($key);
    if (!empty($data)) {
      // update it
      foreach ($value AS $k => $v) {
        $data[$k] = $v;
      }
    }
    else {
      $data = $value;
    }
    // then store it
    $this->tempStore->set($key, $data);
  }

  public function delete(string $key): bool {
    $this->tempStore->delete($key);
  }

  public function keys(): array {
    $query = $this->db->select('key_value_expire', 'kve');
    $query->addField('kve', 'name');
    $query->condition('collection', 'tempstore.shared.tus-server');
    return $query->execute()->fetchCol();
  }

}
